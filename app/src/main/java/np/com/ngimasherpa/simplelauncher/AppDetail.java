package np.com.ngimasherpa.simplelauncher;

import android.graphics.drawable.Drawable;

/**
 * Created by Unstopable Ngima on 8/18/2016.
 */
public class AppDetail {
    CharSequence label;
    CharSequence name;
    Drawable icon;
}
